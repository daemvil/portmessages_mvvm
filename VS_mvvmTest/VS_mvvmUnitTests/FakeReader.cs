﻿using System;
using VS_mvvmTest.Processor;

namespace VS_mvvmUnitTests
{
    public class FakeReader : IReader
    {
        public event Action<byte[]> DataRecieved;

        public void SendValidMessage()
        {
            byte[] data = {0x45, 0x56, 0x45 ^ 0x56};
            OnDataRecieved(data);
        }

        public void SendInvalidMessage()
        {
            byte[] data = { 0x35, 0x56, 0x45 ^ 0x56 };
            OnDataRecieved(data);
        }

        protected virtual void OnDataRecieved(byte[] data)
        {
            DataRecieved?.Invoke(data);
        }
    }
}
