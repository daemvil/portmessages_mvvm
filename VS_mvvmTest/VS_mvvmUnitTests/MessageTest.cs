﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using VS_mvvmTest.Model;
using VS_mvvmTest.Processor;

namespace VS_mvvmUnitTests
{
    [TestClass]
    public class MessageTest
    {
        private readonly FakeReader reader;

        private readonly Parser parser;

        private Message receivedMessage;

        public MessageTest()
        {
            reader = new FakeReader();
            parser = new Parser(reader);
            parser.Parsed += Parsed;
        }

        [TestMethod]
        public void CheckMessageIsValid()
        {
            reader.SendValidMessage();
            Assert.IsTrue(receivedMessage.isValid);
        }

        [TestMethod]
        public void CheckMessageIsInvalid()
        {
            reader.SendInvalidMessage();
            Assert.IsFalse(receivedMessage.isValid);
        }

        private void Parsed(Message message)
        {
            receivedMessage = message;
        }
    }
}
