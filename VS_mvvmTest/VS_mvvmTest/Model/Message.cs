﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VS_mvvmTest.Model
{
    public class Message
    {
        public byte Checksum { get; private set; }
        public byte[] Data { get; private set; }
        public byte Id { get; private set; }
        public bool IsValid { get; private set; }
        public byte Value { get; private set; }
        public DateTime Time { get; private set; }
        public Message(byte id, byte value, byte checksum, byte[] data)
        {
            this.Time = DateTime.Now;
            this.Id = id;
            this.Value = value;
            this.Checksum = checksum;
            this.Data = data;
            this.IsValid = !(id < 0x41 || id > 0x5A || checksum != (id ^ value)); ;
        }

    }
}
