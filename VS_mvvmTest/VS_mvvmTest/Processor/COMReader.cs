﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Messaging;
using System.Runtime.CompilerServices;
using System.Windows;

namespace VS_mvvmTest.Processor
{
    public class COMReader: IReader
    {
        private SerialPort serialPort = new SerialPort();
        public event Action<byte[]> DataRecieved;

        protected void OnDataReceived(byte[] data)
        {
            DataRecieved?.Invoke(data);
        }
        
        public void OnRecieve(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                int dataLength = serialPort.BytesToRead;
                if (dataLength < 3) return; // размер принимаемых данных не менее 3 байт

                byte[] data = new byte[dataLength];
                serialPort.Read(data, 0, dataLength);
                OnDataReceived(data);
                         
            }
            catch (Exception ex)
            {
               MessageBox.Show(ex.ToString());
            }
        }

        public COMReader(string portName = "COM1", Parity parity = Parity.None, Handshake handshake = Handshake.None, int dataBits = 8, StopBits stopBits = StopBits.One, int baudrate = 9600)
        {
            serialPort.PortName = portName;
            serialPort.BaudRate = baudrate;
            serialPort.Parity = parity;
            serialPort.Handshake = handshake;
            serialPort.DataBits = dataBits;
            serialPort.StopBits = stopBits;
            //serialPort.ReadTimeout = 200;
            //serialPort.WriteTimeout = 50;

            serialPort.DataReceived += new SerialDataReceivedEventHandler(OnRecieve);

            serialPort.Open();
        }
    }
}
