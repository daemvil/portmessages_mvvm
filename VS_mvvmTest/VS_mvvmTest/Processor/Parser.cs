﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VS_mvvmTest.Model;

namespace VS_mvvmTest.Processor
{
    public class Parser
    {
        public event Action<Message> Parsed;

        protected void OnParsed(Message message)
        {
            Parsed?.Invoke(message);
        }
        void OnDataRecieved(byte[] data)
        {
            Message message = new Message(data[0], data[1], data[2], data);
            OnParsed(message);
        }
        public Parser(IReader Reader)
        {
            Reader.DataRecieved += OnDataRecieved;
        }

    }
}
