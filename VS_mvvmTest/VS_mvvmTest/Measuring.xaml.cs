﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Input;
using System.Data;
using System.IO.Ports;
using System.IO;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Data;

namespace VS_mvvmTest
{
    /// <summary>
    /// Interaction logic for Measuring.xaml
    /// </summary>
    public partial class Measuring : UserControl
    {
        public enum SortBy { Time, ID, Value };
        public ViewModel.MessageViewModel ViewModel;
        public Measuring()
        {
            InitializeComponent();
        }

        ListSortDirection? direction = null;
        private string prevHeader = string.Empty;

        private void HeaderClick(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;

            if (headerClicked != null)
            {
                if (headerClicked.Role != GridViewColumnHeaderRole.Padding)
                {
                    string header = headerClicked.Column.Header as string;

                    if (direction == null) direction = ListSortDirection.Ascending;
                    else
                    {
                        if (direction == ListSortDirection.Ascending) direction = ListSortDirection.Descending;
                        else direction = ListSortDirection.Ascending;
                    }
                    if (prevHeader != header)
                    {
                        prevHeader = header;
                        direction = ListSortDirection.Ascending;
                    }

                        Application.Current.Dispatcher.Invoke(() => ViewModel.Messages.Sort(header, direction.Value), DispatcherPriority.DataBind);
                }

            }
        }

    }
}