﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Input;
using System.Data;
using System.IO.Ports;
using System.IO;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using VS_mvvmTest.ViewModel;
using System.Xml;

namespace VS_mvvmTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MessageViewModel ViewModel;
        public MainWindow()
        {
            InitializeComponent();
            ViewModel = new MessageViewModel();
            Measuring.ViewModel = ViewModel;
            Measuring.DataContext = ViewModel;
        }
    }
}
