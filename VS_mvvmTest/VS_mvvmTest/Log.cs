﻿using System;
using System.Text;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Reflection;
using VS_mvvmTest.Model;

namespace VS_mvvmTest
{
    public class Log
    {
        private static object sync = new object();
        public static void Write(Message message)
        {
                string pathToLog = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log");
                if (!Directory.Exists(pathToLog))
                    Directory.CreateDirectory(pathToLog); 
                string filename = Path.Combine(pathToLog, string.Format("{0}_{1:dd.MM.yyy}.log",
                AppDomain.CurrentDomain.FriendlyName, DateTime.Now));
                string fullText = string.Format("[{0:HH:mm:ss}] [ID:{1} Value:{2} Checksum:{3}]\r\n",
                message.Time, message.Id, message.Value, message.Checksum);
                lock (sync)
                {
                    File.AppendAllText(filename, fullText, Encoding.GetEncoding("Windows-1251"));
                }
        }
    }
}
