﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections;
using VS_mvvmTest.Processor;
using VS_mvvmTest.Model;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Windows;
using System.ComponentModel;
using System.Windows.Controls;
using static VS_mvvmTest.Measuring;

namespace VS_mvvmTest.ViewModel
{
    public class ObservableMessageCollection: ObservableCollection<Message>
    {
        private static SortBy sorting;
        private static int direction = 0; // ListSortDirection.Ascending
        private static string prevHeader = string.Empty;
        private class sortAscendingComparer : IComparer<Message>
        {
            public int Compare(Message a, Message b)
            {
                switch (sorting)
                {
                    case SortBy.Time:
                        return direction * DateTime.Compare(a.Time, b.Time);
                    case SortBy.ID:
                        return direction * a.Id.CompareTo(b.Id);
                    case SortBy.Value:
                        return direction * a.Value.CompareTo(b.Value);
                    default:
                        return 0;
                }
            }
        }
        public void Sort(string header, ListSortDirection Direction)
        {
            direction = -1 * ((int)Direction * 2 - 1);
            switch (header)
            {
                case "Время":
                    sorting = SortBy.Time;
                    break;
                case "ID":
                    sorting = SortBy.ID;
                    break;
                case "Значение":
                    sorting = SortBy.Value;
                    break;
                default:
                    direction = 1;
                    sorting = SortBy.Time;
                    break;
            }
            var sortableList = new List<Message>(this);
            sortableList.Sort(new sortAscendingComparer());

            for (int i = 0; i < sortableList.Count; i++)
            {
                Move(IndexOf(sortableList[i]), i);
            }
        }

        public void AddSorted(Message message)
        {
            sortAscendingComparer comparer = new sortAscendingComparer();
            int i = 0;
            for (; i < this.Count; i++)
            {
                if (comparer.Compare(message, this[i]) < 0) { break; }
            }
            Insert(i, message);
        }

    }
    public class LogViewModel
    {
        private void OnParsed(Message message)
        {
            Log.Write(message);
        }
        public LogViewModel(Parser Parser)
        {
            Parser.Parsed += OnParsed;
        }
    }
    public class MessageViewModel
    {
        COMReader Reader;
        Parser Parser;
        LogViewModel Logger;
        
        public ObservableMessageCollection Messages { get; }
        
        public MessageViewModel()
        {
            Reader = new COMReader(portName:"COM1");
            Parser = new Parser(Reader);
            Messages = new ObservableMessageCollection();

            Logger = new LogViewModel(Parser);

            Parser.Parsed += OnParsed;
        }

        private void OnParsed(Message message)
        {
            Application.Current.Dispatcher.Invoke(() => Messages.AddSorted(message), DispatcherPriority.DataBind);
        }
    }
}
